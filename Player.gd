extends Area2D
signal hit

export (int) var speed  # How fast the player will move (pixels/sec).
export (bool) var hide
var screensize  # Size of the game window.
var velocity = Vector2() # The player's movement vector.
var analog_velocity = Vector2(0,0)

func _ready():
	screensize = get_viewport_rect().size
	if hide:
		hide()

func _process(delta):
	velocity = Vector2()

	# Add in analog_velocity
	velocity += analog_velocity	

	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		$AnimatedSprite.play()
	else:
		$AnimatedSprite.stop()
	
	position += velocity * delta
	position.x = clamp(position.x, 0, screensize.x)
	position.y = clamp(position.y, 0, screensize.y)

	if velocity.x != 0:
		$AnimatedSprite.animation = "right"
		$AnimatedSprite.flip_v = false
		$AnimatedSprite.flip_h = velocity.x < 0
		$Trail_up.emitting = false
		$Trail_down.emitting = false
		$Trail_left.emitting = false
		$Trail_right.emitting = true
	elif velocity.y != 0:
		if velocity.y < 0:
			$AnimatedSprite.animation = "up"
			$AnimatedSprite.flip_v = velocity.y > 0
			$Trail_up.emitting = true
			$Trail_down.emitting = false
			$Trail_left.emitting = false
			$Trail_right.emitting = false
		elif velocity.y > 0:
			$AnimatedSprite.animation = "down"
			#$AnimatedSprite.flip_v = velocity.y > 0
			$Trail_up.emitting = false
			$Trail_down.emitting = true
			$Trail_left.emitting = false
			$Trail_right.emitting = false

func mov_up():
	var velocity = Vector2() # The player's movement vector.
	velocity.y -= 1
	pass
func mov_down():
	var velocity = Vector2() # The player's movement vector.
	velocity.y += 1
	pass
	
func mov_left():
	var velocity = Vector2() # The player's movement vector.
	velocity.x -= 1
	pass
func mov_right():
	var velocity = Vector2() # The player's movement vector.
	velocity.x += 1
	pass

func analog_force_change(inForce, inStick):
	if(inStick.get_name()=="Analog"):
		if (inForce.length() < 0.1):
			analog_velocity = Vector2(0,0) 
		else:
			analog_velocity = Vector2(inForce.x,-inForce.y)
		analog_velocity = analog_velocity.normalized()
		analog_velocity.x = stepify(analog_velocity.x, 1)
		analog_velocity.y = stepify(analog_velocity.y, 1)



func _on_Player_body_entered(body):
	hide() # Player disappears after being hit.
	emit_signal("hit")
	$CollisionShape2D.disabled = true

func start(pos):
	position = pos
	show()
	$CollisionShape2D.disabled = false
