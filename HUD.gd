extends CanvasLayer

signal start_game
signal up
signal down
signal right
signal left
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func show_message(text):
	$MessageLabel.text = text
	$MessageLabel.show()
	$MessageTimer.start()

func show_game_over():
	show_message("¡OUCH!")
	yield($MessageTimer, "timeout")
	$StartButton.show()
	$MessageLabel.text = "¡Esquiva a los malos!"
	$MessageLabel.show()

func update_score(score):
	$ScoreLabel.text = str(score)

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_MessageTimer_timeout():
	$MessageLabel.hide()


func _on_StartButton_pressed():
	$StartButton.hide()
	emit_signal("start_game")


func _on_Up_pressed():
	emit_signal("up")
	pass # replace with function body


func _on_Down_pressed():
	emit_signal("down")
	pass # replace with function body


func _on_Left_pressed():
	emit_signal("left")
	pass # replace with function body


func _on_Right_pressed():
	emit_signal("right")
	pass # replace with function body
